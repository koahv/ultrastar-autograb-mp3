// ==UserScript==
// @name         URL Redirect
// @namespace    http://tampermonkey.net/
// @version      2024-01-09
// @description  go directly to countdown page
// @author       Al
// @match        https://usdb.animux.de/*
// @icon         https://www.google.com/s2/favicons?sz=64&domain=animux.de
// @grant        none
// ==/UserScript==

var links = document.getElementsByTagName('a');
for (var i = 0; i < links.length; i++) {
  links[i].href = links[i].href.replace('detail', 'gettxt');
}