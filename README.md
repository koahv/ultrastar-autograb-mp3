# UltraStar AutoGrab-MP3

## Instructions

Clone this repo, place txt's in the assets folder and run go.sh. Requires yt-dlp.

UserScript for use with usdb.animux.de included to rewrite detail page links directly to countdown page. Requires Tampermonkey or equivalent.
