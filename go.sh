##########################################################
# place txt's in assets folder folder and run the script #
##########################################################

# CONFIG
process_youtube_links=false  # 
include_mp3=true            # download mp3
include_video=true          # download music video
video_resolution=480        # max video resolution eg 1080 720 480 360 etc
add_video_path_to_txt=false  # link the downloaded video in the txt. Overwrites any previous video config.
cleanup_downloads=false      # remove unused downloads
convert_to_utf_8=true       # convert unknown-8bit txt's to utf-8

# set working dir to location of script
cd "$(dirname "$0")" || exit
# set count to 0
count=0
missing_mp3s=()
missing_mp4s=()

for f in assets/*.txt; do 
  echo "processing $f..."
  # get base file name
  bname=$(basename "$f" .txt)
  # extract video data from txt
  vid=$(grep -Po "(?<=\VIDEO|COMMENT).*?(?=\,|$)" "$f")
  # code only
  value=${vid#*=}
  if [[ "$process_youtube_links" = true ]]; then
    if [[ ! -f "assets/$bname.mp4" ]]; then
      yt-dlp -S ext:mp4 -f "bv*[height<=$video_resolution]" --recode mp4 https://www.youtube.com/watch?v="$value" -o "assets/$bname-orig.%(ext)s"
    fi
   # yt-dlp -f "bv*[height<=$video_resolution]+ba/b"
  fi

  if [[ "$process_youtube_links" = true ]] && [[ "$include_mp3" = true ]]; then
    #  extract mp3 stream
    if [[ ! -f "assets/$bname".mp3 ]]; then
      yt-dlp --extract-audio https://www.youtube.com/watch?v="$value" --audio-format mp3 --audio-quality 0  -o "assets/$bname.%(ext)s"
    fi
    #ffmpeg -i "assets/$bname"-orig.mp4 -c:v copy -c:a libmp3lame -q:a 4 "assets/$bname".mp3
  fi

  if [[ "$process_youtube_links" = true ]] && [[ "$include_video" = true ]]; then
    # extract mp4 stream
    if [[ ! -f "assets/$bname.mp4" ]]; then
          ffmpeg -i "assets/$bname-orig.mp4" -c copy -an "assets/$bname.mp4"
    fi
  fi

  if [[ "$cleanup_downloads" = true ]]; then
    if [[ -f "assets/$bname"-orig.mp4 ]]; then
      rm assets/"$bname"-orig.mp4
    fi
  fi

  if [[ "$add_video_path_to_txt" = true ]]; then
    # replace original video line with comment
    sed -i 's/#VIDEO\:v\=:/#COMMENT:/' "$f"
    # remove any previous video >> comment processing
    sed -i '/COMMENT.*mp4\|VIDEO.*mp4/d' "$f"
    # add mp4 into video line after mp3
    sed -i "/#MP3:/a #VIDEO:$bname.mp4" "$f"
  fi

  if [[ "$convert_to_utf_8" = true ]]; then
    # if txt encoding is unknown-8bit
    if [[ $(file -i "$f" | grep -Po 'charset=\K.*') == "unknown-8bit" ]]; then
      # convert to utf-8
      iconv -f "windows-1252" -t "UTF-8" -o "$f.new" "$f" && mv -f "$f.new" "$f"
      echo "converted to utf8" 
    fi
  fi

  if [[ ! -f "assets/$bname.mp3" ]]; then
    missing_mp3s+=("assets/$bname.mp3")
  fi

  if [[ ! -f "assets/$bname.mp4" ]]; then
    missing_mp4s+=("assets/$bname.mp4")
  fi

  # track progress
  ((count++))

done

txtcount=$(find . -mindepth 1 -type f -name "*.txt" -printf x | wc -c)
mp3count=$(find . -mindepth 1 -type f -name "*.mp3" -printf x | wc -c)
mp4count=$(find . -mindepth 1 -type f -name "*.mp4" -printf x | wc -c)


#echo "processed $count iterations. dir contains $mp3count mp3's from $txtcount txt's"

echo "$txtcount":"$mp3count":"$mp4count" | sed '1i txts_processed:mp3s:mp4s\n' | column -s: -t

echo "MISSING MP3s"
printf '%s\n' "${missing_mp3s[@]}"
echo "MISSING MP4s"
printf '%s\n' "${missing_mp4s[@]}"
